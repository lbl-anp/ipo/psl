# psl

PSL is a software framework for localizing radiation point sources in a 3D environment using a
freely moving radiation detector system. The framework assumes a single point source of
radiation is in the environment and uses known detector positions and orientations along with
their radiation measurements to find the most likely location of the point source. This
approach to the problem enables analyses that improve upon the capability of a standard
Maximum Likelihood Expectation Maximization (MLEM) approach, such as setting a source
detection threshold, finding a likelihood interval for the position of the source, finding a
likelihood interval for the activity (intensity) of the source, determining whether more than
one point source may be present, and generating a map of Minimum Detectable Activity (MDA)
for a given source type. Applications include radiological and nuclear security, consequence
management, decontamination and decommissioning of nuclear facilities, and radiological
emergency response.

## licensing

For licensing questions please visit https://ipo.lbl.gov. Disclosure 2020-020.
